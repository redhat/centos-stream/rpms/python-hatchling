#!/bin/bash -eux

# hatchling 1.24+ requires uv and others for testing - they're not available
# in our systems, hence PyPI installation
# install the specific version of packages that have been released around the time
# the tested hatchling's version was out, just to be sure it works
# we don't rebase packages often in CentOS Stream, so version constraints
# should be reasonably safe to be hardcoded
pip install uv==0.1.35 editables==0.5 rich==13.9.4 httpx==0.27.2

# Get the version of our installed component
VERSION=$(rpm -q --queryformat="%{VERSION}" python3-hatchling)

# The hatch's source code contains the tests for hatchling in tests/backend
git clone --depth 1 --branch hatchling-v$VERSION https://github.com/pypa/hatch/
cd hatch

# Delete the hatchling sources from the repository - we want to test the RPM package
rm -r backend/

# Work around tests wanting a proper Hatch installation
echo '__version__ = "(bogus)"' > src/hatch/_version.py

PYTHONPATH="$PWD/src" python3 -m pytest tests/backend -v
